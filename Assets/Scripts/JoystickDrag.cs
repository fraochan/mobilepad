﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class JoystickDrag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private RectTransform m_DraggingPlane;
    private Vector3 initialPosition;
    private float minX = 0.0f;
    private float maxX = 0.0f;
    private float minY = 0.0f;
    private float maxY = 0.0f;
    float padWidth = 0.0f;
    float padHeight = 0.0f;
    private RectTransform currentPosition;
    private float minDistAction = 35;
    Hermes hermes;
    Canvas JoystickCanvas;
    MovementDirection lastMovement;
    MovementDirection movement;
    bool dragging = false;
    int minDistMovement = 45;

    void Start()
    {
        hermes = Hermes.GetInstance();
        initialPosition = new Vector3(GetComponent<RectTransform>().position.x, GetComponent<RectTransform>().position.y, 0.0f);
        currentPosition = GetComponent<RectTransform>();
        JoystickCanvas = FindInParents<Canvas>(gameObject);
        padWidth = currentPosition.rect.width / 2;
        padHeight = currentPosition.rect.height / 2;
        float canvasWidth = JoystickCanvas.GetComponent<RectTransform>().rect.width;
        float canvasHeight = JoystickCanvas.GetComponent<RectTransform>().rect.height;

        minX = padWidth / 2;
        minY = padHeight / 2;
        maxX = canvasWidth - (padWidth / 2);
        maxY = canvasHeight - (padHeight / 2);

        lastMovement = new MovementDirection()
        {
            horizontal = "Idle",
            vertical = "Center"
        };
        movement = new MovementDirection();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (JoystickCanvas == null)
            return;
        currentPosition = GetComponent<RectTransform>();
    }

    public void OnDrag(PointerEventData eventData)
    {
        SetDraggedPosition(eventData);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        currentPosition.position = JoystickCanvas.transform.TransformPoint(new Vector2(initialPosition.x + padWidth, initialPosition.y + padHeight));
        sendActionToPlayer();
    }

    private void SetDraggedPosition(PointerEventData data)
    {
        dragging = true;
        float clampedX = Mathf.Clamp(data.position.x, minX, maxX);
        float clampedY = Mathf.Clamp(data.position.y, minY, maxY);

        currentPosition.position = JoystickCanvas.transform.TransformPoint(new Vector2(clampedX, clampedY));
        sendActionToPlayer();
    }

    private void sendActionToPlayer()
    {
        Vector3 initialPosLocal = JoystickCanvas.transform.InverseTransformPoint(initialPosition);
        Vector3 currPosLocal = JoystickCanvas.transform.InverseTransformPoint(currentPosition.position);

        movement.horizontal = "Idle";
        movement.vertical = "Center";
        if (Mathf.Abs(Vector3.Distance(initialPosLocal, currPosLocal)) > minDistAction)
        {
            if (currPosLocal.x > initialPosLocal.x) // Right
            {
                if ((currPosLocal.x - initialPosLocal.x) > minDistMovement)
                {
                    movement.horizontal = "Right";
                }
            }
            else // Left
            {
                if ((initialPosLocal.x - currPosLocal.x) > minDistMovement)
                {
                    movement.horizontal = "Left";
                }
            }
            if (currPosLocal.y > initialPosLocal.y) // Up
            {
                if ((currPosLocal.y - initialPosLocal.y) > minDistMovement)
                {
                    movement.vertical = "Up";
                }
            }
            else // Down
            {
                if ((initialPosLocal.y - currPosLocal.y) > minDistMovement)
                {
                    movement.vertical = "Down";
                }
            }
        }
        hermes.PostNotification(this, "MovePlayer", movement);
        dragging = false;
    }

    static public T FindInParents<T>(GameObject go) where T : Component
    {
        if (go == null) return null;
        var comp = go.GetComponent<T>();

        if (comp != null)
            return comp;

        Transform t = go.transform.parent;
        while (t != null && comp == null)
        {
            comp = t.gameObject.GetComponent<T>();
            t = t.parent;
        }
        return comp;
    }

    public class MovementDirection
    {
        public string horizontal { get; set; }
        public string vertical { get; set; }
    }
}

