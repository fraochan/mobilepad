﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Gestures;
using static JoystickDrag;

public class PlayerController : MonoBehaviour
{

    Hermes hermes;
    MovementDirection currentMovement;
    MovementDirection lastMovement;
    float velX = 7f;
    float velYJump = 7f;
    float velXJump = 5.5f;
    float velSign = 0.0f;
    float dashSpeed = 0.0f;
    Rigidbody2D rb;
    float minX, maxX;
    int jumps = 0;
    bool isJumping = false, isDashing = false, isRunning = false;
    int maxJumps = 2;
    Animator animator;
    bool currDirRight = true;
    public GameObject ShootBlast;
    float blastSpeedX = 10.0f;
    float blastDistanceX = 1.1f;
    float blastDistanceY = 0.15f;
    bool keepShooting = false;
    float timeLastShoot = 0.0f;
    float timeBetweenShoots = 0.3f;
    

    // Start is called before the first frame update
    void Start()
    {
        hermes = Hermes.GetInstance();
        hermes.AddObserver(this, "MovePlayer");
        hermes.AddObserver(this, "StopPlayer");
        hermes.AddObserver(this, "ProcessGesture");
        rb = GetComponent<Rigidbody2D>();
        jumps = 0;
        animator = GetComponent<Animator>();
        currentMovement = new MovementDirection();
        lastMovement = new MovementDirection() { horizontal = "Idle", vertical = "Center" };
    }

    void Update()
    {
        if (!isJumping || isDashing)
            rb.velocity = new Vector2(velX * (velSign + dashSpeed), GetComponent<Rigidbody2D>().velocity.y);

        if (keepShooting)
        {
            Shoot();
        }
    }

    public void MovePlayer(Notification notification)
    {
        MovementDirection data = (MovementDirection)notification.data;
        currentMovement.horizontal = data.horizontal;
        currentMovement.vertical = data.vertical;
        if (!isJumping || currentMovement.horizontal=="Idle") // Movement left or right is forbidden while jumping
        {
            HorizontalMovement();
            VerticalMovement();
            animator.SetBool("Running", isRunning);
            animator.ResetTrigger(lastMovement.horizontal);
            animator.ResetTrigger(lastMovement.vertical);
            animator.SetTrigger(currentMovement.horizontal);
            animator.SetTrigger(currentMovement.vertical);
            lastMovement.vertical = currentMovement.vertical;
            lastMovement.horizontal = currentMovement.horizontal;
        }
    }

    void HorizontalMovement()
    {
        switch (currentMovement.horizontal)
        {
            case "Right":
                velSign = 1.0f;
                isRunning = true;
                currDirRight = true;
                break;
            case "Left":
                velSign = -1.0f;
                isRunning = true;
                currDirRight = false;
                break;
            default:
                velSign = 0.0f;
                isRunning = false;
                break;
        }
        
    }

    void VerticalMovement()
    {
        switch(currentMovement.vertical)
        {
            case "Up":
                break;
            case "Down":
                velSign = 0.0f;
                break;
            default:
                break;
        }
        
    }
    
    void ProcessGesture(Notification notification)
    {
        GestureInformation gesture = (GestureInformation)notification.data;
        switch(gesture.gesture)
        {
            case "Jump":
                Jump(gesture);
                break;
            case "Dash":
                Dash(gesture);
                break;
            case "Click":
                if(!keepShooting) Shoot();
                break;
            case "DoubleClick":
                FixShooting();
                break;
            default:
                break;
        }        
    }

    void Jump(GestureInformation gesture)
    {
        if (gesture.gestureSampling[0].y > gesture.gestureSampling[gesture.gestureSampling.Count - 1].y) return; // Only jumps bottom up
        if (jumps < maxJumps)
        {
            animator.SetTrigger("Jump");
            animator.SetBool("Jumping", true);
            rb.velocity = new Vector2(velXJump * velSign, velYJump);
            jumps++;
            isJumping = true;
        }
    }

    void Dash(GestureInformation gesture)
    {
        if (((currDirRight && isDashingRight(gesture)) || (!currDirRight && !isDashingRight(gesture))) && lastMovement.vertical!="Down")
        {
            isDashing = true;
            dashSpeed = currDirRight ? 3.0f : -3.0f;
            animator.SetTrigger("Dash");
            Invoke("FinishDash", 0.15f);
        }
    }

    bool isDashingRight(GestureInformation gesture)
    {
        if (gesture.gestureSampling[0].x < gesture.gestureSampling[gesture.gestureSampling.Count - 1].x) return true;
        return false;
    }

    void FinishDash()
    {
        isDashing = false;
        dashSpeed = 0.0f;
        animator.ResetTrigger("Dash");
        if (!isJumping)
            animator.SetTrigger(lastMovement.horizontal);
        else
            animator.SetTrigger("Idle");
        if(isJumping)
        {
            // If we are still jumping update speed to 0 in x
            rb.velocity = new Vector2(velX * (velSign + dashSpeed), GetComponent<Rigidbody2D>().velocity.y);
        }
    }

    void Shoot()
    {
        var time = Time.time;

        if (time - timeLastShoot > timeBetweenShoots)
        {

            float posX = blastDistanceX;
            float posY = blastDistanceY;
            float velX = blastSpeedX;
            if (!currDirRight)
            {
                posX = -posX;
                velX = -velX;
            }

            GameObject blast = Instantiate(ShootBlast, new Vector3(transform.position.x + posX, transform.position.y + posY, 1.0f), Quaternion.identity);
            if (!currDirRight) blast.GetComponent<SpriteRenderer>().flipX = true;
            Rigidbody2D rbBlast = blast.GetComponent<Rigidbody2D>();
            rbBlast.velocity = new Vector2(velX, 0.0f);
            timeLastShoot = time;
        }
    }

    void FixShooting()
    {
        keepShooting = !keepShooting;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            jumps = 0;
            isJumping = false;
            animator.SetBool("Jumping", false);
        }
    }

}
