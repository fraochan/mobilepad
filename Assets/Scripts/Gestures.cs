﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Gestures : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler
{
    List<Vector3> positions = null;
    GestureRecognizer gestureRecognizer = null;
    bool dragging = false;
    GestureInformation foundGesture;
    GestureInformation lastGesture;
    Hermes hermes;
    float clicked = 0;
    float clicktime = 0;
    float clickdelay = 0.5f;

    void Start()
    {
        gestureRecognizer = FindObjectOfType<GestureRecognizer>();
        hermes = Hermes.GetInstance();
        lastGesture = new GestureInformation() { gesture = "" };
        positions = new List<Vector3>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        foundGesture = new GestureInformation();
        positions.Clear();
        positions.Add(eventData.position);
        dragging = true;
    }

    public void OnDrag(PointerEventData eventData)
    {
        positions.Add(eventData.position);
    }

    public void OnPointerDown(PointerEventData data)
    {
        
    }



    public void OnPointerUp(PointerEventData eventData)
    {
        if(!dragging || positions.Count<=2)
        {
            foundGesture = new GestureInformation()
            {
                gesture = "Click",
                accuracy = 0.0f
            };
            hermes.PostNotification(this, "ProcessGesture", foundGesture);
            lastGesture.gesture = foundGesture.gesture;

            clicked++;
            if (clicked == 1) clicktime = Time.time;

            if (clicked > 1 && ((Time.time - clicktime) < clickdelay) && lastGesture.gesture == "Click")
            {
                clicked = 0;
                clicktime = 0;
                foundGesture = new GestureInformation()
                {
                    gesture = "DoubleClick",
                    accuracy = 0.0f
                };
                hermes.PostNotification(this, "ProcessGesture", foundGesture);
                lastGesture.gesture = foundGesture.gesture;
            }
            else if (clicked > 2 || Time.time - clicktime > clickdelay || lastGesture.gesture != "Click") clicked = 0;

        }

    }

    public void OnEndDrag(PointerEventData eventData)
    {
        positions.Add(eventData.position);
        if (positions.Count<=2) // too few positions is considered a click
        {
            foundGesture = new GestureInformation()
            {
                gesture = "Click",
                accuracy = 0.0f
            };
        }
        else
        {
            Tuple<string, float> resultGest = gestureRecognizer.recognizeGesture(positions);
            foundGesture = new GestureInformation()
            {
                gesture = resultGest.Item1,
                gestureSampling = positions,
                accuracy = resultGest.Item2
            };
        }
        hermes.PostNotification(this, "ProcessGesture", foundGesture);
        lastGesture.gesture = foundGesture.gesture;
        positions.Clear();
        dragging = false;
    }


    public class GestureInformation
    {
        public string gesture { get; set; }
        public List<Vector3> gestureSampling { get; set; }
        public float accuracy { get; set; }
    }
}
