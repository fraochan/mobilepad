﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestureInterpreter : MonoBehaviour
{
    // Start is called before the first frame update
    Hermes hermes;
    void Start()
    {
        hermes = Hermes.GetInstance();
        hermes.AddObserver(this, "NewGesture");
    }

    void NewGesture(Notification notification)
    {
        Tuple<string, float> gesture = (Tuple<string, float>)notification.data;
        hermes.PostNotification(this, "ShowGestureText", gesture.Item1);
    }   

}
