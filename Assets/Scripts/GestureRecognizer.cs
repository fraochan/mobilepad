﻿using System;
using System.Collections.Generic;
using System.IO;
using PDollarGestureRecognizer;
using QDollarGestureRecognizer;
using UnityEngine;

public class GestureRecognizer : MonoBehaviour
{
    private Gesture[] trainingSet = null;   
        
    void Start()
    {
        trainingSet = LoadTrainingSet();
    }
        
    private Gesture[] LoadTrainingSet()
    {
        List<Gesture> gestures = new List<Gesture>();
        /*string[] gestureFolders = Directory.GetDirectories(Application.streamingAssetsPath + "\\Gestures");
        foreach (string folder in gestureFolders)
        {
            string[] gestureFiles = Directory.GetFiles(folder, "*.xml");
            foreach (string file in gestureFiles)
                gestures.Add(GestureIO.ReadGesture(file));
        }*/
        gestures.Add(GestureIO.ReadGesture("Dash"));
        gestures.Add(GestureIO.ReadGesture("Jump"));
        return gestures.ToArray();
    }

    public Tuple<string, float> recognizeGesture(List<Vector3> gesturePoints)
    {

        List<Point> points = new List<Point>();

        foreach (Vector3 vector in gesturePoints)
        {
            points.Add(ConvertVector3ToPoint(vector));
        }

        Gesture gesture = new Gesture(points.ToArray());
        Tuple<string, float> foundGesture = QPointCloudRecognizer.Classify(gesture, trainingSet);

        return foundGesture;
    }

    private Point ConvertVector3ToPoint(Vector3 vector)
    {
        Point point = new Point(vector.x, vector.y, 0);
        return point;
    }
        
}
