﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GestureText : MonoBehaviour
{
    Hermes hermes;
    Text gestureText;
    // Start is called before the first frame update
    void Start()
    {
        hermes = Hermes.GetInstance();
        hermes.AddObserver(this, "ShowGestureText");
        gestureText = GetComponent<Text>();
    }

    void ShowGestureText(Notification notification)
    {
        string text = (string)notification.data;
        gestureText.text = text;
    }
}
