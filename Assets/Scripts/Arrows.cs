﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Arrows : MonoBehaviour
{
    Hermes hermes;
    
    void Start()
    {
        hermes = Hermes.GetInstance();
        hermes.AddObserver(this, "ShowArrow");
        hermes.AddObserver(this, "DisableAllArrows");
    }

    private void ShowArrow(Notification notification)
    {
        string arrow = (string)notification.data;
        DisableAllArrows();
        var temp = GameObject.Find(arrow);
        if (temp) {
            Image image = temp.GetComponent<Image>();
            if (image != null)
            {
                var tempColor = image.color;
                tempColor.a = 1f;
                image.color = tempColor;
            }
        }
    }

    private void DisableAllArrows()
    {
        foreach(Transform child in transform)
        {
            Image image = child.GetComponent<Image>();
            if (image != null)
            {
                var tempColor = image.color;
                tempColor.a = 0f;
                image.color = tempColor;
            }
        }
    }
    
}
